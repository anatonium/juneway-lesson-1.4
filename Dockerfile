FROM debian:9 as build

RUN apt update && apt install -y wget gcc make libreadline-dev libpcre3-dev libssl-dev patch
RUN mkdir /tmp/nginx-build 
WORKDIR /tmp/nginx-build
# Downloading all crap
RUN wget 'http://nginx.org/download/nginx-1.14.1.tar.gz' && wget 'http://luajit.org/download/LuaJIT-2.0.4.tar.gz' && wget -O nginx_devel_kit.tar.gz https://github.com/simpl/ngx_devel_kit/archive/v0.3.0.tar.gz && wget -O nginx_lua_module.tar.gz https://github.com/openresty/lua-nginx-module/archive/v0.10.8.tar.gz
RUN tar xvf LuaJIT-2.0.4.tar.gz && tar xvf nginx-1.14.1.tar.gz && tar xvf nginx_devel_kit.tar.gz && tar xvf nginx_lua_module.tar.gz
# patch luajit source with fix
RUN wget https://raw.githubusercontent.com/macports/macports-ports/09603eacd0424538f0321e2ce04f5295f987571a/www/nginx/files/patch-src-ngx_http_lua_headers.c.diff 
RUN patch ./lua-nginx-module-0.10.8/src/ngx_http_lua_headers.c patch-src-ngx_http_lua_headers.c.diff
# building luajit & nginx
WORKDIR /tmp/nginx-build/LuaJIT-2.0.4
RUN make && make install
WORKDIR /tmp/nginx-build/nginx-1.14.1
ENV LUAJIT_LIB=/usr/local/lib 
ENV LUAJIT_INC=/usr/local/include/luajit-2.0 
RUN ./configure --add-module=/tmp/nginx-build/ngx_devel_kit-0.3.0 --add-module=/tmp/nginx-build/lua-nginx-module-0.10.8 --without-mail_imap_module --without-mail_pop3_module --without-http_gzip_module && make && make install

FROM debian:9

WORKDIR /usr/local/nginx/
COPY --from=build /usr/local/nginx/ .
RUN touch ./logs/error.log && chmod +x ./sbin/nginx
# I realised, that forgot something important
COPY nginx.conf /usr/local/nginx/conf/nginx.conf
COPY --from=build /usr/local/lib/libluajit-5.1.a /usr/local/lib/
COPY --from=build /usr/local/lib/libluajit-5.1.so /usr/local/lib/
COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /usr/local/lib/
COPY --from=build /usr/local/lib/libluajit-5.1.so.2.0.4 /usr/local/lib/
# enviroment variable with location of luajit
ENV LD_LIBRARY_PATH=/usr/local/lib

WORKDIR /usr/local/nginx/sbin
CMD ["./nginx", "-g", "daemon off;"]
